import React from 'react'
import get from 'lodash.get';
import {PostsContainer} from 'vactory-gatsby-forum'

export const ListContainer = ({data}) => {
    const pageCount = get(data, 'components.0.data_count', 0);
    let components = {}
    components.title = get(data, 'components.0.title', '')
    components.link = get(data, 'components.0.link.url', null)
    components.link_label = get(data, 'components.0.link.title', '')
    components.show_link = get(data, 'components.0.show_link', null)
    components.is_listing = get(data, 'components.0.is_listing', null)

    return (
      <PostsContainer
        components={components}
        nodes={data.data.nodes}
        categories={data.data.all_categories}
        pageCount={pageCount}
      />
    );
};
