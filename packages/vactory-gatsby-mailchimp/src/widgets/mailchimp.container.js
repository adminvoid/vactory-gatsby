import React, { useState } from 'react'
import get from 'lodash.get'
import { Toast } from "vactory-gatsby-ui";
import { useTranslation } from 'react-i18next'
import Api from "vactory-gatsby-api"
import qs from "qs"
import { useForm } from "react-hook-form";


export const MailchimpWidgetContainer = ({ data }) => {
	const { t, i18n } = useTranslation();
	const [loading, setLoading] = useState(false)
	const { register, handleSubmit, setError, errors, reset } = useForm();

	const list_id = get(data, 'components.0.mailchimp_list.id', null);
	const title = get(data, 'extra_field.title', null);
	const description = get(data, 'extra_field.intro.value.#text', null);
	// const link = get(data, 'extra_field.link.url', null);
	// const link_label = get(data, 'extra_field.link.title', null);

	const onSubmit = values => {
		values.id = list_id

		setLoading(true)
		const { hide } = Toast.loading("Loading...", { hideAfter: 0 });

		Api.postRest(
			'_mailchimp',
			qs.stringify(values),
			i18n.language,
			{
				headers: {
					"content-type": "application/x-www-form-urlencoded"
				}
			}
		)
			.then(function (response) {
				setLoading(false)
				hide();
				Toast.success(t("Vous vous êtes enregistré avec succès."));
				console.log(response)
				reset();
			})
			.catch(function (error) {
				setLoading(false)
				hide();

				if (error?.response?.data?.type === 'system') {
					Toast.error(error?.response?.data?.message || t("Une erreur s'est produite."));
				}
				else if (error?.response?.data?.type === 'field') {
					// Field error.
					setError(error?.response?.data?.type?.field || 'email', {
						type: "manual",
						message: error?.response?.data?.message || t("Une erreur s'est produite.")
					});
				}
				else {
					Toast.error(t("Une erreur s'est produite."));
				}
			})
	};


	return (
		<div className="bg-white">
			<div className="relative sm:py-16">
				<div aria-hidden="true" className="hidden sm:block">
					<div className="absolute inset-y-0 left-0 w-1/2 bg-gray-50 rounded-r-3xl" />
					<svg
						className="absolute top-8 left-1/2 -ml-3"
						width={404}
						height={392}
						fill="none"
						viewBox="0 0 404 392"
					>
						<defs>
							<pattern
								id="8228f071-bcee-4ec8-905a-2a059a2cc4fb"
								x={0}
								y={0}
								width={20}
								height={20}
								patternUnits="userSpaceOnUse"
							>
								<rect
									x={0}
									y={0}
									width={4}
									height={4}
									className="text-gray-200"
									fill="currentColor"
								/>
							</pattern>
						</defs>
						<rect
							width={404}
							height={392}
							fill="url(#8228f071-bcee-4ec8-905a-2a059a2cc4fb)"
						/>
					</svg>
				</div>
				<div className="mx-auto max-w-md px-4 sm:max-w-3xl sm:px-6 lg:max-w-7xl lg:px-8">
					<div className="relative rounded-2xl px-6 py-10 bg-indigo-600 overflow-hidden shadow-xl sm:px-12 sm:py-20">
						<div
							aria-hidden="true"
							className="absolute inset-0 -mt-72 sm:-mt-32 md:mt-0"
						>
							<svg
								className="absolute inset-0 h-full w-full"
								preserveAspectRatio="xMidYMid slice"
								xmlns="http://www.w3.org/2000/svg"
								fill="none"
								viewBox="0 0 1463 360"
							>
								<path
									className="text-indigo-500 text-opacity-40"
									fill="currentColor"
									d="M-82.673 72l1761.849 472.086-134.327 501.315-1761.85-472.086z"
								/>
								<path
									className="text-indigo-700 text-opacity-40"
									fill="currentColor"
									d="M-217.088 544.086L1544.761 72l134.327 501.316-1761.849 472.086z"
								/>
							</svg>
						</div>
						<div className="relative">
							<div className="sm:text-center">
								{title && (
									<h2 className="text-3xl font-extrabold text-white tracking-tight sm:text-4xl">
										{title}
									</h2>
								)}
								{description && <div className="mt-6 mx-auto max-w-2xl text-lg text-indigo-200" dangerouslySetInnerHTML={{ __html: description }} />}
							</div>
							<form
								onSubmit={handleSubmit(onSubmit)}
								className="mt-12 sm:mx-auto sm:max-w-4xl sm:flex"
							>
								<div className="min-w-0 flex-1">
									<label
										htmlFor="cta_email"
										className="sr-only"
									>
										{t('Votre mail')}
									</label>
									<input
										id="cta_email"
										type="email"
										name="email"
										className={`block w-full border border-transparent rounded-md px-5 py-3 text-base text-gray-900 placeholder-gray-500 shadow-sm focus:outline-none focus:border-transparent focus:ring-2 focus:ring-white focus:ring-offset-2 focus:ring-offset-indigo-600 ${errors.email ? 'border-red-300' : 'border-transparent'}`}
										placeholder={t('Renseignez votre adresse e-mail')}
										ref={register({ required: t("Ce champ est requis") })}
									/>
									{errors.email && <p className="mt-2 text-sm text-red-600">{errors.email.message || errors.email?.types?.message}</p>}
								</div>
								<div className="mt-4 sm:mt-0 sm:ml-3">
									<button
										type="submit"
										disabled={loading}
										className="block w-full rounded-md border border-transparent px-5 py-3 bg-indigo-500 text-base font-medium text-white shadow hover:bg-indigo-400 focus:outline-none focus:ring-2 focus:ring-white focus:ring-offset-2 focus:ring-offset-indigo-600 sm:px-10"
									>
										{t('Je m’inscris à la newsletter')}
									</button>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	);

};
