import React, { useContext } from "react"
import { useForm } from "react-hook-form"
import ReCaptcha from "react-google-recaptcha"
import { useTranslation } from "react-i18next"
import Api from "vactory-gatsby-api"
import { AppSettings, AuthContext } from "vactory-gatsby-core"
// import {Box, Label, Input, Button, Text} from "vactory-ui"
import { Toast } from 'vactory-gatsby-ui';

export const CommentForm = ({ entity_uid, cid = null, type_content, title = 'Poster un commentaire' }) => {
    const { isAuthenticated, getUserProfile, getAuthorisationHeader } = useContext(AuthContext);
    const { t, i18n } = useTranslation();
    const currentLanguage = i18n.language;
    const { handleSubmit, register, clearError, errors, setValue, reset } = useForm();
    const url = `${Api.baseURL}${currentLanguage}/api/comment/comment`;
    const recaptchaRef = React.useRef();
    const user = getUserProfile();
    const isLoggedin = isAuthenticated();

    const CreateData = (params) => {
        let parent = null;
        if (params.parent) {
            parent = {
                "type": `comment--comment`,
                "id": params.parent,
            };
        }

        let comment = {
            "data": {
                "type": `comment--comment`,
                "attributes": {
                    // "status": true,
                    "subject": params.body.slice(0, 10),
                    "name": params.name,
                    // "mail": this.state.email,
                    "entity_type": "node",
                    "field_name": "comment",
                    "g-recaptcha-response": params.captcha,
                    "comment_body": {
                        "value": params.body,
                        "format": "plain_text",
                    },
                },
                "relationships": {
                    "entity_id": {
                        "data": {
                            "type": `node--${type_content}`,
                            "id": params.entity_id,
                        },
                    },
                    "pid": {
                        "data": parent,
                    },
                },
            },
        }

        if (isLoggedin) {
            comment = {
                ...comment,
                data: {
                    ...comment.data,
                    uid: {
                        type: 'user--user',
                        id: user.uuid
                    }
                }
            }
        }

        return comment
    };

    const onSubmit = values => {
        const { hide } = Toast.loading("Loading...", { hideAfter: 0 });
        let headers = {
            "Content-Type": "application/vnd.api+json",
            "Accept": "application/vnd.api+json",
        };

        if (isLoggedin) {
            headers.Authorization = getAuthorisationHeader()
        }

        const comment = CreateData({
            // name: isAuthenticated ? null : values.name,
            name: values.name,
            body: values.body,
            captcha: values.captcha_response,
            entity_id: entity_uid,
            parent: cid
        });

        //POST Query to save comment
        Api.kitsu.axios({
            method: "POST",
            url: url,
            headers: headers,
            data: comment,
        })
            .then(res => {
                reset();
                recaptchaRef.current.reset();
                hide();
                Toast.success(t("Votre commentaire à été ajouter."));
            })
            .catch(err => {
                recaptchaRef.current.reset();
                hide();
                Toast.error(t("Une erreur s'est produite"));
            })
    };

    return (
        <form onSubmit={handleSubmit(onSubmit)}>
            <input type="hidden" name="captcha_response"
                ref={register({ required: t("Le champs Captcha est requis") })} />
                <div className="bg-gray-50 p-8 w-full mt-10 mb-10 relative">

                    <h2 className="text-gray-900 text-lg mb-1 font-medium title-font">{title}</h2>

                    {!isLoggedin &&
                        <div className="relative mb-4">
                            <label htmlFor="comment-name"
                                className={`leading-7 text-sm ${errors.name ? 'text-red-600' : 'text-gray-600'}`}
                            >{t('Votre nom')} <span>*</span></label>
                            <input
                                id="comment-name"
                                name="name"
                                type="text"
                                placeholder={t("Votre nom")}
                                className={`w-full bg-white rounded border focus:border-indigo-500 focus:ring-2 focus:ring-indigo-200 text-base outline-none py-1 px-3 leading-8 transition-colors duration-200 ease-in-out ${errors.name ? 'border-red-300' : 'border-gray-300'}`}
                                ref={register({ required: t("Le champs 'Votre nom' est requis") })} />
                            {errors.name &&
                                <p className="mt-2 text-sm text-red-600">
                                    {errors.name.message}
                                </p>}
                        </div>}
                    <div className="relative mb-4">
                        <label htmlFor="message"
                            className={`leading-7 text-sm ${errors.name ? 'text-red-600' : 'text-gray-600'}`}
                        >{t('Message')} <span>*</span></label>
                        <textarea
                            id="message"
                            ref={register({ required: t("Le champs 'Votre commentaire' est requis") })}
                            name="body"
                            placeholder={t("Votre commentaire")}
                            className={`w-full bg-white rounded border focus:border-indigo-500 focus:ring-2 focus:ring-indigo-200 h-32 text-base outline-none text-gray-700 py-1 px-3 resize-none leading-6 transition-colors duration-200 ease-in-out ${errors.name ? 'border-red-300' : 'border-gray-300'}`} />
                        {errors.body &&
                            <p className="mt-2 text-sm text-red-600">
                                {errors.body.message}
                            </p>
                        }
                    </div>
                    <div className="relative mb-4">
                        <div className="flex justify-between items-center">
                            <div>
                                <ReCaptcha
                                    sitekey={AppSettings.keys.reCaptcha}
                                    hl={currentLanguage}
                                    ref={recaptchaRef}
                                    onChange={val => {
                                        setValue("captcha_response", val);
                                        clearError("captcha_response");
                                    }}
                                    onExpired={() => {
                                        setValue("captcha_response", null)
                                    }}
                                    onErrored={() => {
                                        setValue("captcha_response", null)
                                    }}
                                />
                                {errors.captcha_response &&
                                    <p className="mt-2 text-sm text-red-600">{errors.captcha_response.message}</p>
                                }
                            </div>
                            <button className="text-white bg-indigo-500 border-0 py-2 px-6 focus:outline-none hover:bg-indigo-600 rounded text-lg">{t('Envoyer')}</button>
                        </div>
                    </div>
                </div>
        </form>
    )
};

