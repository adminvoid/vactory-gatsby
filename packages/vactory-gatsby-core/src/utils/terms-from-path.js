export const findTermsFromPath = (param_value, terms = [], default_value = 'all') => {
    if (typeof param_value === 'undefined') {
        return default_value
    }
    const param_terms = param_value.split("+")

    const result = []
    for (let x of param_terms) {
        const item = terms.find(item => item.slug === x);
        if (item) {
            result.push(item.id);
        }
    }

    if (result.length <= 0) {
        return default_value
    }

    return result
}

export const findTermsSlugFromIds = (value, terms = [], default_value = 'all') => {
    const result = terms
    .filter(item => value.includes(item.id))
    .map(item => item.slug)
    .join("+")

    if (result.length <= 0) {
        return default_value
    }

    return result
}