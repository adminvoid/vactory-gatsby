import React from "react"
import {Comments} from 'vactory-gatsby-comments'
// import { imageLayoutStyles } from 'vactory-gatsby-academy'
// import { Picture } from 'vactory-gatsby-ui'
import { VCCBlock } from "vactory-gatsby-vcc"
import { normalizeNodes } from "vactory-gatsby-forum"
import { useTranslation } from "react-i18next";
import {ForumCard} from 'vactory-gatsby-forum'

const Post = ({ post, _VCC }) => {

  const { t } = useTranslation();
  return (
    <div>
    <div className="container">
      <div>
        <div>
        <h1 className="mt-10 mb-10 block text-3xl leading-8 font-extrabold tracking-tight text-gray-900 sm:text-4xl">{post.title}</h1>
          <div
            dangerouslySetInnerHTML={{
              __html: post.body,
            }}
          />
        </div>
        {/* <div>
          <div>
            <div>
              <div>
                <div>
                {post.forum_expert.picture &&
                  <Picture
                    file={post.forum_expert.picture}
                    sizes={imageLayoutStyles.Avatar.sizes}
                    alt={post.forum_expert.name}
                    width={imageLayoutStyles.Avatar.width}
                    height={imageLayoutStyles.Avatar.height}
                    ratio={imageLayoutStyles.Avatar.ratio}
                    style={{
                      mb: "16px",
                      borderRadius: "50%",
                    }}
                  />}
                </div>
                <div>
                  <div>
                    <h5>
                      {post.forum_expert.first_name}{" "}
                      {post.forum_expert.last_name}
                    </h5>
                    <p>
                      {post.forum_expert.profession}
                    </p>
                  </div>
                </div>
              </div>
            </div>
            <div>
              <p>{post.forum_expert.about}</p>
            </div>
          </div>
        </div> */}
      </div>
      <Comments
        entity_uid={post.id}
        type_content="vactory_forum"
        uid={parseInt(post.forum_expert.id)}
      />
    </div>
    <VCCBlock
                config={_VCC}
                title={t("Voir D’autres Discussion Sur La Même Thématique")}
                linkLabel={t("Voir toutes les discussions")}
                normalizer={normalizeNodes}
                renderNode={(node) => <ForumCard item={node} />}
                wrapperClass=""
                itemClass="w-full p-10 bg-white"
            />
    </div>
  );
  }
export default Post;
