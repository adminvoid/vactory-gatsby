const Api = require('vactory-gatsby-api');
const chalk = require("chalk");
const path = require("path");
const fse = require("fs-extra");

const esmRequire = require('esm')(module);
const appConfig = esmRequire(path.join(process.cwd(), "gatsby-vactory-config.js")).default;

exports.onPreBootstrap = async () => {
    // Get frequent searches.
    console.log(chalk.green("[\u2713] Frequent Searches"));
    let keywords = {};
    try {
        const response = await Api.getRest('frequent-searches', {
            params: { index: 'default_content_index', limit: 10, lang: appConfig.languages.defaultLanguage},
        }, appConfig.languages.defaultLanguage)
        keywords = {keywords: response.data.keywords}
    } catch (error) {
        console.error(error)
    }

    // Save translations
    const frequentSearchesFile = `${__dirname}/.tmp/frequent-searches.json`;
    try {
        await fse.ensureFile(frequentSearchesFile);
        await fse.writeJson(frequentSearchesFile, keywords);
    } catch (err) {
        console.error(err)
    }

};
