import React, { useState, useContext, useRef, useEffect } from "react"
import { useTranslation } from 'react-i18next'
import { Transition } from '@headlessui/react'
import { useForm } from "react-hook-form"
import { AppSettings, AuthContext } from "vactory-gatsby-core"
import { Toast } from 'vactory-gatsby-ui';
import ReCaptcha from "react-google-recaptcha"
import Api from "vactory-gatsby-api"

export const CreateNewPost = ({ isOpen, closeLayer, terms = [] }) => {
    const { t, i18n } = useTranslation();
    const container = useRef(null);
    const { isAuthenticated, getUserProfile, getAuthorisationHeader } = useContext(AuthContext);
    const currentLanguage = i18n.language;
    const { handleSubmit, register, clearError, errors, setValue, reset, watch } = useForm();
    const url = `${Api.baseURL}${currentLanguage}/api/node/vactory_forum`;
    const recaptchaRef = React.useRef();
    const user = getUserProfile();
    const isLoggedin = isAuthenticated();
    const watchDescription = watch("forum_description", "");
    const [loading, setLoading] = useState(false)

    useEffect(() => {
        const handleOutsideClick = (event) => {
            if (container !== null && !container?.current?.contains(event.target)) {
                if (!isOpen) return;
                closeLayer();
            }
        };

        window.addEventListener('click', handleOutsideClick);
        return () => window.removeEventListener('click', handleOutsideClick);
    }, [isOpen, container]); // eslint-disable-line react-hooks/exhaustive-deps

    useEffect(() => {
        const handleEscape = (event) => {
            if (!isOpen) return;

            if (event.key === 'Escape') {
                closeLayer();
            }
        };

        document.addEventListener('keyup', handleEscape);
        return () => document.removeEventListener('keyup', handleEscape);
    }, [isOpen]); // eslint-disable-line react-hooks/exhaustive-deps

    const CreateData = (params) => {

        let requestBody = {
            "data": {
                "type": `node--vactory_forum`,
                "attributes": {
                    "title": params.title,
                    "node_summary": params.title,
                    "body": {
                        "value": params.body,
                        "format": "plain_text"
                    },
                    "field_vactory_excerpt": {
                        "value": params.body.slice(0, 300),
                        "format": "plain_text"
                    },
                    "g-recaptcha-response": params.captcha,
                },
                "relationships": {
                    "uid": {
                        "data": {
                            "type": "user--user",
                            "id": user.uuid
                        }
                    },
                    "field_forum_editeur": {
                        "data": {
                            "type": "user--user",
                            "id": user.uuid
                        }
                    },
                    "field_forum_expert": {
                        "data": {
                            "type": "user--user",
                            "id": user.uuid
                        }
                    },
                    "field_vactory_taxonomy_1": {
                        "data": {
                            "type": "taxonomy_term--forum_section",
                            "id": params.category
                        }
                    }
                }
            },
        }

        return requestBody
    };

    const onSubmit = values => {
        const { hide } = Toast.loading("Loading...", { hideAfter: 0 });
        setLoading(true)

        let headers = {
            "Content-Type": "application/vnd.api+json",
            "Accept": "application/vnd.api+json",
        };

        if (isLoggedin) {
            headers.Authorization = getAuthorisationHeader()
        }

        const comment = CreateData({
            title: values.forum_title,
            body: values.forum_description,
            category: values.forum_category,
            captcha: values.captcha_response,
        });

        //POST Query to save comment
        Api.kitsu.axios({
            method: "POST",
            url: url,
            headers: headers,
            data: comment,
        })
            .then(res => {
                reset();
                recaptchaRef.current.reset();
                hide();
                setLoading(false)
                Toast.success(t("Votre discussion à été ajouter."));
            })
            .catch(err => {
                recaptchaRef.current.reset();
                hide();
                setLoading(false)
                Toast.error(t("Une erreur s'est produite"));
            })

    }

    return (
        <Transition show={isOpen}>
            <div className="fixed z-20 inset-0 overflow-hidden">
                <div className="absolute inset-0 overflow-hidden">
                    <section ref={container} className="absolute inset-y-0 pl-16 max-w-full right-0 flex" aria-labelledby="slide-over-heading">
                        <Transition.Child
                            className="w-screen max-w-2xl"
                            enter="transform transition ease-in-out duration-700"
                            enterFrom="translate-x-full"
                            enterTo="translate-x-0"
                            leave="transform transition ease-in-out duration-700"
                            leaveFrom="translate-x-0"
                            leaveTo="translate-x-full"
                        >

                            <form
                                onSubmit={handleSubmit(onSubmit)}
                                className="h-full divide-y divide-gray-200 flex flex-col bg-white shadow-xl">
                                <input type="hidden" name="captcha_response"
                                    ref={register({ required: t("Ce champs est requis") })} />

                                <div className="flex-1 h-0 overflow-y-auto">
                                    <div className="py-6 px-4 bg-indigo-700 sm:px-6">
                                        <div className="flex items-center justify-between">
                                            <h2 id="slide-over-heading" className="text-lg font-medium text-white">
                                                {t('Nouvelle discussion')}
                                            </h2>
                                            <div className="ml-3 h-7 flex items-center">
                                                <button
                                                    type="button"
                                                    onClick={closeLayer}
                                                    className="bg-indigo-700 rounded-md text-indigo-200 hover:text-white focus:outline-none focus:ring-2 focus:ring-white">
                                                    <span className="sr-only">{t('Fermer')}</span>
                                                    {/* Heroicon name: outline/x */}
                                                    <svg className="h-6 w-6" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true">
                                                        <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M6 18L18 6M6 6l12 12" />
                                                    </svg>
                                                </button>
                                            </div>
                                        </div>
                                        <div className="mt-1">
                                            <p className="text-sm text-indigo-300">
                                                Get started by filling in the information below to create your new project.
                </p>
                                        </div>
                                    </div>
                                    <div className="flex-1 flex flex-col justify-between">
                                        <div className="px-4 divide-y divide-gray-200 sm:px-6">
                                            <div className="space-y-6 pt-6 pb-5">
                                                <div>
                                                    <label
                                                        htmlFor="forum_title"
                                                        className={`block text-sm font-medium ${errors.forum_title ? 'text-red-900' : 'text-gray-900'}`}
                                                    >
                                                        {t('Titre')}
                                                    </label>
                                                    <div className="mt-1">
                                                        <input
                                                            type="text"
                                                            name="forum_title"
                                                            id="forum_title"
                                                            className={`block w-full shadow-sm sm:text-sm focus:ring-indigo-500 focus:border-indigo-500 rounded-md ${errors.forum_title ? 'border-red-300' : 'border-gray-300'}`}
                                                            ref={register({
                                                                required: t("Ce champ est requis"),
                                                                minLength: {
                                                                    value: 10,
                                                                    message: t("Le titre doit comporter au moins 10 caractères.")
                                                                },
                                                                maxLength: {
                                                                    value: 150,
                                                                    message: t("La valeur maximale du titre ne doit pas dépasser 150 caractères.")
                                                                }
                                                            })}
                                                        />
                                                        {errors.forum_title &&
                                                            <p className="mt-2 text-sm text-red-600">
                                                                {errors.forum_title.message}
                                                            </p>}
                                                    </div>
                                                </div>

                                                <div>
                                                    <label
                                                        htmlFor="forum_category"
                                                        className={`block text-sm font-medium ${errors.forum_category ? 'text-red-900' : 'text-gray-900'}`}
                                                    >
                                                        {t('Thématique')}
                                                    </label>
                                                    <div className="mt-1">
                                                        <select
                                                            id="forum_category"
                                                            name="forum_category"
                                                            className={`block pl-3 pr-10 py-2 text-base focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm rounded-md ${errors.forum_category ? 'border-red-300' : 'border-gray-300'}`}
                                                            ref={register({
                                                                required: t("Ce champ est requis"),
                                                            })}
                                                        >
                                                            <option value="">{t("-- Sélectionner --")}</option>
                                                            {terms.map((term) => (
                                                                <option key={term.uuid} value={term.uuid}>
                                                                    {term.name}
                                                                </option>
                                                            ))}
                                                        </select>
                                                        {errors.forum_category &&
                                                            <p className="mt-2 text-sm text-red-600">
                                                                {errors.forum_category.message}
                                                            </p>}
                                                    </div>
                                                </div>
                                                <div>
                                                    <label
                                                        htmlFor="forum_description"
                                                        className={`block text-sm font-medium ${errors.forum_description ? 'text-red-900' : 'text-gray-900'}`}
                                                    >
                                                        {t('Description')}
                                                    </label>
                                                    <div className="mt-1">
                                                        <textarea
                                                            id="forum_description"
                                                            name="forum_description"
                                                            rows={7}
                                                            className={`block w-full shadow-sm sm:text-sm focus:ring-indigo-500 focus:border-indigo-500 rounded-md ${errors.forum_description ? 'border-red-300' : 'border-gray-300'}`}
                                                            ref={register({
                                                                required: t("Ce champ est requis"),
                                                                minLength: {
                                                                    value: 20,
                                                                    message: t("La description doit comporter au moins 20 caractères.")
                                                                },
                                                                maxLength: {
                                                                    value: 700,
                                                                    message: t("La valeur maximale de la description ne doit pas dépasser 700 caractères.")
                                                                }
                                                            })}
                                                        />
                                                        <div className="count mt-1 text-gray-400 text-xs font-semibold">{watchDescription.length}/700</div>
                                                        {errors.forum_description &&
                                                            <p className="mt-2 text-sm text-red-600">
                                                                {errors.forum_description.message}
                                                            </p>}
                                                    </div>
                                                </div>
                                                <div>
                                                    <div className="mt-1">
                                                        <ReCaptcha
                                                            sitekey={AppSettings.keys.reCaptcha}
                                                            hl={currentLanguage}
                                                            ref={recaptchaRef}
                                                            onChange={val => {
                                                                setValue("captcha_response", val);
                                                                clearError("captcha_response");
                                                            }}
                                                            onExpired={() => {
                                                                setValue("captcha_response", null)
                                                            }}
                                                            onErrored={() => {
                                                                setValue("captcha_response", null)
                                                            }}
                                                        />
                                                        {errors.captcha_response &&
                                                            <p className="mt-2 text-sm text-red-600">{errors.captcha_response.message}</p>
                                                        }
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="pt-4 pb-6">
                                                <div className="mt-4 flex text-sm">
                                                    <a href="#." className="group inline-flex items-center text-gray-500 hover:text-gray-900">
                                                        {/* Heroicon name: solid/question-mark-circle */}
                                                        <svg className="h-5 w-5 text-gray-400 group-hover:text-gray-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                                                            <path fillRule="evenodd" d="M18 10a8 8 0 11-16 0 8 8 0 0116 0zm-8-3a1 1 0 00-.867.5 1 1 0 11-1.731-1A3 3 0 0113 8a3.001 3.001 0 01-2 2.83V11a1 1 0 11-2 0v-1a1 1 0 011-1 1 1 0 100-2zm0 8a1 1 0 100-2 1 1 0 000 2z" clipRule="evenodd" />
                                                        </svg>
                                                        <span className="ml-2">
                                                            {t('Lire les engagements des utilisateurs du Forum')}
                                                        </span>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="flex-shrink-0 px-4 py-4 flex justify-end">
                                    <button type="button" onClick={closeLayer} className="bg-white py-2 px-4 border border-gray-300 rounded-md shadow-sm text-sm font-medium text-gray-700 hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                                        {t('Annuler')}
                                    </button>
                                    <button type="submit" className={`ml-4 inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500 ${loading ? 'cursor-not-allowed' : ''}`} disabled={loading}>
                                        {loading && <svg className="animate-spin -ml-1 mr-3 h-5 w-5 text-white" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24">
  <circle className="opacity-25" cx={12} cy={12} r={10} stroke="currentColor" strokeWidth={4} />
  <path className="opacity-75" fill="currentColor" d="M4 12a8 8 0 018-8V0C5.373 0 0 5.373 0 12h4zm2 5.291A7.962 7.962 0 014 12H0c0 3.042 1.135 5.824 3 7.938l3-2.647z" />
</svg>
}
                                        {t('Créer')}
                                    </button>
                                </div>
                            </form>
                        </Transition.Child>
                    </section>
                </div>
            </div>
        </Transition>
    )
}