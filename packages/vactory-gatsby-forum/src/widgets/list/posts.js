import React from 'react'
import {ForumCard} from 'vactory-gatsby-forum'

const Posts = ({ posts }) => {
  return (
    <ul className="relative z-0 divide-y divide-gray-200 border-b border-gray-200" >
      {posts.map((item, idx) => {
        return (
          <li key={idx}>
            <ForumCard item={item} />
          </li>
        );

      })}
    </ul>
  )

}

export default Posts
