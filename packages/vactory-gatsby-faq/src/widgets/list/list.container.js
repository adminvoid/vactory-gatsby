import React, { useState, useEffect, useRef, useContext } from "react";
import get from 'lodash.get';
import { Pagination } from "vactory-gatsby-ui";
import { useTranslation } from "react-i18next";
import Api from "vactory-gatsby-api";
import {
	postsQueryParams,
	normalizeNodes,
	normalizeDFNodes,
	FormFilter,
	PostsPage,
} from "vactory-gatsby-faq";
import {PageContext, findTermsFromPath, findTermsSlugFromIds} from 'vactory-gatsby-core'

export const ListContainer = ({ data }) => {
    const { t, i18n } = useTranslation();
    const currentLanguage = i18n.language;
    const {params, internalMatchPath, isMatchPath} = useContext(PageContext);
    const nodes = get(data, 'components.0.views.data.nodes', []);
    const pageCount = get(data, 'components.0.views.data.count', 0);
    const terms = get(data, 'components.0.views.data.exposed.faq_section', []);
    const isFirstRun = useRef(isMatchPath ? false : true);
    const normalizedNodes = normalizeDFNodes(isMatchPath ? [] : nodes);
    const [isLoading, setIsLoading] = useState(false);
    const [posts, setPosts] = useState(normalizedNodes);
    const [selectedTerms, setSelectedTerms] = useState(isMatchPath ? findTermsFromPath(params.theme, terms, 'all') : 'all');
    const [pager, setPager] = useState(1);
    const [count, setCount] = useState(isMatchPath ? 0 : pageCount);

    const fetchData = React.useCallback(() => {
        let categoryFilter = {
            "filter[category][condition][path]": "field_vactory_taxonomy_1.drupal_internal__tid",
            "filter[category][condition][operator]": "IN",
            "filter[category][condition][value]": selectedTerms,
        };

        if (selectedTerms === "all") {
            categoryFilter = {};
        }

        const requestParams = {
            ...postsQueryParams,
            page: { limit: postsQueryParams.page.limit, offset: (pager - 1) * postsQueryParams.page.limit },
            ...categoryFilter,
        };

        setIsLoading(true);

        Api.getResponse("node/faq", requestParams, currentLanguage)
            .then((res) => {
                const normalizedNodes = normalizeNodes(res.data);
                setPosts(normalizedNodes);
                setIsLoading(false);
                setCount(res.meta.count);
            })
            .catch((err) => {
                setIsLoading(false);
                console.log(err);
            });
    }, [selectedTerms, currentLanguage, pager])

    const updateURLHistory = (section) => {
        // Update URL history.
        if (internalMatchPath) {
            let url = internalMatchPath;
            url = url.replace(':theme', findTermsSlugFromIds(section, terms, 'all' ) || 'all')
            window.history.pushState(null, null, url)
        }
    }

    const handlePaginationChange = (selected) => {
        setPager(selected);
    };

    const handleReset = () => {
        setSelectedTerms('all'); 
        updateURLHistory('all');
		setPager(1);
    }

    const handleChange = (selectedSection) => {

		setSelectedTerms(selectedSection);
		setPager(1);
		updateURLHistory(selectedSection);
    }

    useEffect(() => {
        if (isFirstRun.current) {
            isFirstRun.current = false;
            return;
        }

        fetchData();
    }, [pager, fetchData])


	return (
		<div className="container- max-w-5xl mx-auto px-5">
			<FormFilter
				terms={terms}
				selectedTerms={selectedTerms}
                handleChange={handleChange}
                handleReset={handleReset}
				isLoading={isLoading}
			/>


			{posts.length > 0 && (
				<PostsPage posts={posts} />
			)}

			{!isLoading && posts.length <= 0 && (
				<p className="text-center my-8">
					{t("Aucun résultat trouvé")}
				</p>
			)}

			{count > postsQueryParams.page.limit && (
				<Pagination
					total={count}
					pageSize={postsQueryParams.page.limit}
					current={pager}
					onChange={handlePaginationChange}
				/>
			)}
		</div>
	);
};
