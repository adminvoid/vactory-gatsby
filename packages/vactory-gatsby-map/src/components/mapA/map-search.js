import React, { useState } from "react";
import Fuse from "fuse.js";
import { useTranslation } from "react-i18next";
import { SearchResult } from "./searchComponents";
import { Icon } from "vactory-gatsby-ui";

function paginator(arr, perPage) {
	if (perPage < 1 || !arr) return () => [];

	return function (page) {
		const basePage = page * perPage;

		return page < 0 || basePage >= arr.length
			? []
			: arr.slice(basePage, basePage + perPage);
	};
}

const FuzeOptions = {
	shouldSort: true,
	threshold: 0.1, // perfect match.
	// location: 0,
	// distance: 100,
	minMatchCharLength: 3,
	keys: [
		{
			name: "field_locator_adress.locality",
			weight: 0.3,
		},
		{
			name: "field_locator_adress.address_line1",
			weight: 0.2,
		},
		{
			name: "name",
			weight: 0.1,
		},
	],
};

export const MapSearch = ({ items, onSelect }) => {
	const { t } = useTranslation();
	const [results, setResults] = useState([]);
    const inputREf = React.useRef(null);
	const [pageCount, setPageCount] = useState(0);
	const [pageNumber, setPageNumber] = useState(0); // eslint-disable-line no-unused-vars
	const pageLimit = 10;
	const currentResults = paginator(results, pageLimit)(pageNumber);
    const [openSearchLayer, setOpenSearchLayer] = useState(false);
    const smallScreen = window.matchMedia('(max-width: 480px)');

	const onSearch = (e) => {
		const value = e.target.value;
		let suggestions = [];

		if (value.length > 0) {
			suggestions = new Fuse(items, FuzeOptions).search(value);
			setPageCount(Math.ceil(suggestions.length / pageLimit));

			setOpenSearchLayer(true);
		}
		setResults(suggestions);
	};
	const onSearchClick = () => {
        setOpenSearchLayer(true);
	};

	const onCloseSearchLayer = (event) => {
		event.preventDefault();
        setOpenSearchLayer(false);
        if (inputREf.current)
            inputREf.current.value = "";
    };
    
    const onResultClick = (event, item) => {
        onSelect(null, item);
        if (smallScreen.matches) {
            setOpenSearchLayer(false)
        }
    }

	return (
		<div className="absolute left-0 top-0 md:left-4 md:top-4 z-1 w-full sm:w-96 bg-white">
			<div className="flex justify-between bg-white py-2 px-4">
                <input
                    ref={inputREf}
					type="text"
					placeholder="Search"
					className="form-control border-0 flex-grow p-0 text-gray-500 ring-0 focus:ring-0"
					onChange={onSearch}
					onClick={onSearchClick}
				/>
				<div className="space-x-4 rtl:space-x-reverse flex">
					{currentResults.length > 0 && (
						<button
							onClick={onCloseSearchLayer}
							className="text-gray-400 hover:text-black flex items-center justify-center"
						>
							<Icon icon="close" className="w-3 h-3" />
						</button>
					)}
					<button className="text-gray-400 hover:text-black flex items-center justify-center">
						<Icon icon="search" className="w-3 h-3" />
					</button>
				</div>
			</div>

			{openSearchLayer && (
				<div className="map-search-result-wrapper border-t border-black">
					{currentResults.length > 0 && (
						<div className="map-search-result overflow-y-auto py-2 max-h-80">
							{currentResults.map(({ item }, index) => (
								<div className="py-1 px-3 hover:bg-gray-100 cursor-pointer">
									<SearchResult
										key={index + item.name}
										name={item.name}
										locality={item?.field_locator_adress?.locality}
										addressLine1={item?.field_locator_adress?.address_line1}
										addressLine2={item?.field_locator_adress?.address_line2}
										onClick={(e) => onResultClick(e, item)}
									/>
								</div>
							))}
						</div>
					)}
					{pageCount > 0 && currentResults.length > 0 && (
						<div className="map-search-pagination flex items-center justify-between px-4 py-2 text-sm">
							<span>
								{results.length} {t("Résultat (s)")}
							</span>
							<span>
								{pageNumber + 1} {t("of")} {pageCount}
							</span>
						</div>
					)}
				</div>
			)}
		</div>
	);
};
