import React from "react";
import { useTranslation } from "react-i18next";
import { useForm, Controller } from "react-hook-form";
import Select from "react-select";
import { LoadingSpinner } from "mre";

const CustomStyles = {
	menuPortal: (base) => ({
		...base,
		zIndex: 9999,
	}),
	container: (base) => ({
		...base,
		// flex: "1 1 0%",
		height: "100%",
		width: "100%",
	}),
	control: (base) => ({
		...base,
		height: "100%",
		borderRadius: 0,
		border: 0,
	}),
};

export const FormFilter = ({ terms, selectedTerms, handleChange, handleReset, isLoading }) => {
	const { t } = useTranslation();
	const { handleSubmit, control, setValue } = useForm();

	const options = terms.map((term) => {
		return {
			label: term.label,
			value: term.id,
		};
	});

	const onReset = () => {
		setValue("section", "all");
		handleReset();
	};

	React.useEffect(() => {
		setValue(
			"section",
			options.filter((option) => selectedTerms.includes(option.value))
		);
	}, []); //eslint-disable-line react-hooks/exhaustive-deps

	const onSubmit = (data) => {
		const selectedSection =
			data.section &&
			Array.isArray(data.section) &&
			data.section.length > 0
				? data.section.map((v) => v.value)
				: "all";

		handleChange(selectedSection);
	};
	return (
		<form onSubmit={handleSubmit(onSubmit)}>
			<div className="my-20 flex flex-col md:flex-row space-y-4 md:space-y-0 md:space-x-4 rtl:space-x-reverse isolate">
				<div className="bg-gradient-to-r from-primary via-secondary to-black p-px md:w-80">
					<Controller
						as={
							<Select
								isSearchable={false}
								placeholder={t("Toutes les thématiques")}
								isMulti={true}
								styles={CustomStyles}
								menuPortalTarget={
									typeof document !== "undefined"
										? document.body
										: null
								}
							/>
						}
						isClearable
						options={options}
						onChange={([selected]) => {
							return selected;
						}}
						control={control}
						name="section"
					/>
				</div>

				<button
					type="submit"
					className="inline-flex items-center justify-center px-6 py-3 border border-black relative isolate group"
				>
					<div className="absolute inset-y-0 ltr:right-0 rtl:left-0 w-full group-hover:w-0 transition bg-black" />
					<span className="relative z-1 text-white group-hover:text-black text-base font-medium">
						{t("Appliquer")}
					</span>
				</button>

				<button
					type="reset"
					onClick={onReset}
					className="inline-flex items-center justify-center px-6 py-3 border border-black relative isolate group"
				>
					<div className="absolute inset-y-0 ltr:left-0 rtl:right-0 w-0 group-hover:w-full transition bg-black" />
					<span className="relative z-1 text-black group-hover:text-white text-base font-medium">
						{t("Réinitialiser")}
					</span>
				</button>

				{isLoading && <LoadingSpinner />}
			</div>
		</form>
	);
};
