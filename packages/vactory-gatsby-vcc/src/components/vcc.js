import React, { useEffect, useState } from "react";
import Api from "vactory-gatsby-api";
import { useTranslation } from "react-i18next";
import { Transition } from "@headlessui/react";
import { CardContentLoader } from "vactory-gatsby-ui";
import { mapOrder } from "../utils/mapOrder";

const StandardContainer = ({children}) => {
	return <div>{children}</div>
}
export const VCC = ({
	title,
	linkLabel,
	nid,
	resource,
	resourceType,
	queryParams,
	renderNode,
	renderNodes,
	normalizer,
	limit = 3,
}) => {
	const { t, i18n } = useTranslation();
	const language = i18n.language;
	const [posts, setPosts] = useState(null);
	const [moreLink, setMoreLink] = useState(null);
	let internalTitle = title ? title : t("Articles similaires");
	let internalLinkLabel = linkLabel ? linkLabel : t("Voir plus");

	const renderer = (nodes) => {
		if (renderNodes !== undefined) return renderNodes(nodes);
		else
			return (
				<div className="flex flex-wrap">
					{nodes.map((node) => (
						<Transition
							key={node.id}
							show={true}
							enter="transform ease-out duration-300 transition"
							enterFrom="scale-90 opacity-0"
							enterTo="scale-100 opacity-100"
							leave="transform ease-out duration-300 transition"
							leaveFrom="scale-100 opacity-100"
							leaveTo="scale-90 opacity-0"
							className="w-full sm:w-1/2 md:w-1/3 px-1 sm:px-2: md:px-3"
						>
							{renderNode(node)}
						</Transition>
					))}
				</div>
			);
	};

	useEffect(() => {
		async function loadSimilarIds() {
			const { data } = await Api.getResponse(
				`node/${resource}`,
				{
					filter: {
						drupal_internal__nid: nid,
					},
					fields: {
						[resourceType]: "vcc_normalized",
					},
				},
				language
			);

			return new Promise(function (resolve, reject) {
				if (data[0]["vcc_normalized"]) {
					resolve(data[0]["vcc_normalized"]);
				} else {
					reject(
						`VCC is used in ${resource} but has not been configured for the content type. Check out /admin/structure/types/manage/events`
					);
				}
			});
		}

		async function loadNodes() {
			const config = await loadSimilarIds();
			const ids = config.ids;

			let nidsFilter = {
				"filter[ids][condition][path]": "drupal_internal__nid",
				"filter[ids][condition][operator]": "IN",
				"filter[ids][condition][value]": [],
			};

			ids.forEach((id) => {
				nidsFilter["filter[ids][condition][value]"].push(id);
			});

			const params = {
				...nidsFilter,
				fields: queryParams.fields,
				include: queryParams.include,
				page: {
					limit: config.limit,
				},
			};

			const { data } = await Api.getResponse(
				`node/${resource}`,
				params,
				language
			);
			let sortedNodes = mapOrder(data, ids, "drupal_internal__nid");
			return {
				nodes: sortedNodes,
				config,
			};
		}

		loadNodes()
			.then(({ nodes, config }) => {
				setPosts(normalizer(nodes));
				if (config.link_more && config.link_more.length > 0) {
					setMoreLink(config.link_more);
				}
			})
			.catch((error) => console.warn(error));
	}, [nid]); // eslint-disable-line react-hooks/exhaustive-deps

	return (
		<div className="bg-gray-eee">
			<div className="container">
				<StandardContainer
					title={internalTitle}
					// intro={description}
					body={
						posts
							? renderer(posts)
							: (
							<div className="flex flex-wrap">
								{Array(limit).fill().map((e, i) => (
										<div key={i}
											className="w-full sm:w-1/2 md:w-1/3 px-1 sm:px-2: md:px-3"
										>
											<CardContentLoader />
										</div>
									))}
							</div>
						)
					}
					action={{
						title: internalLinkLabel,
						url: moreLink || "#!",
					}}
				/>
			</div>
		</div>
	);
};
