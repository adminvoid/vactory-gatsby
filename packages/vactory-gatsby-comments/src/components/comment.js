import React from "react"
import { useTranslation } from "react-i18next"
// import {Text, Image} from "vactory-ui";
import * as moment from "moment"
import 'moment/locale/fr';
import 'moment/locale/ar';
import get from 'lodash.get';
import DefaultCommentAvatar from "../images/avatar-default.png"
// import {
//     CommentBase,
//     CommentAvatar,
//     CommentBox,
//     CommentHead,
//     CommentContent,
//     CommentName
// } from "vactory-gatsby-comments"

export const Comment = ({ comment, uid }) => {
    const { t, i18n } = useTranslation();
    const currentLanguage = i18n.language;
    moment.locale(currentLanguage);

    // const [showReplies, setShowReplies] = useState(false);
    const avatar = get(comment, "internal_user.uid.picture._default", DefaultCommentAvatar);
    const timeAgo = moment(get(comment, "created")).fromNow();
    const commentUid = parseInt(get(comment, "internal_user.uid.id", 0));
    const isOwner = commentUid === uid;
    const username = get(comment, "internal_user.uid.name", "No name");
    const first_name = get(comment, "internal_user.uid.first_name");
    const last_name = get(comment, "internal_user.uid.last_name");
    const content = get(comment, "comment_body.processed", '');
    // const hasReplies = get(comment, "extra_data.hasChilds", false);
    // const repliesCount = get(comment, "extra_data.count", 0);

    let fullName = `${first_name} ${last_name}`;

    // Anonymous.
    if (commentUid === 0) {
        fullName = get(comment, 'name', 'Anonymous')
        if (!fullName || fullName === "") {
            fullName = "Anonymous"
        }
    } else if (first_name === "" && last_name === "") {
        fullName = username
        if (!fullName || fullName === "") {
            fullName = "No name"
        }
    }

    return (
        <li className="py-6 flex">
            <span className="inline-block relative">
                <img className="h-10 w-10 rounded-full" src={avatar} alt={fullName} />
                {isOwner && <span title={t('Auteur')} className="absolute top-0 right-0 block h-4 w-4 rounded-full ring-2 ring-white bg-indigo-50 text-indigo-700">
                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                        <path fill="#fff" d="M12 14l9-5-9-5-9 5 9 5z" />
                        <path fill="#fff" d="M12 14l6.16-3.422a12.083 12.083 0 01.665 6.479A11.952 11.952 0 0012 20.055a11.952 11.952 0 00-6.824-2.998 12.078 12.078 0 01.665-6.479L12 14z" />
                        <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M12 14l9-5-9-5-9 5 9 5zm0 0l6.16-3.422a12.083 12.083 0 01.665 6.479A11.952 11.952 0 0012 20.055a11.952 11.952 0 00-6.824-2.998 12.078 12.078 0 01.665-6.479L12 14zm-4 6v-7.5l4-2.222" />
                    </svg>
                </span>}
                
            </span>
            <div className="ml-3">
                <div className="flex justify-between space-x-3">
                    <div className="min-w-0 flex-1">
                        <p className={`text-sm font-medium text-gray-900 ${isOwner ? 'font-bold text-indigo-700' : ''} `}>
                            {fullName}
                        </p>
                    </div>
                    <time className="flex-shrink-0 whitespace-nowrap text-sm text-gray-500">{timeAgo}</time>
                </div>
                <div className="text-sm text-gray-500" dangerouslySetInnerHTML={{ __html: content }} />
            </div>
        </li>
    )

    // return (

    //     <CommentBase>
    //         <CommentAvatar>
    //             <img src={avatar} alt={fullName}/>
    //         </CommentAvatar>
    //         <CommentBox>
    //             <CommentHead>
    //                 <CommentName isOwner={isOwner} ownerText={t('Author')}>{fullName}</CommentName>
    //                 <p>{timeAgo}</p>
    //             </CommentHead>
    //             <CommentContent>
    //                 <p dangerouslySetInnerHTML={{__html: content}}/>
    //             </CommentContent>

    //             {/*{hasReplies &&*/}
    //             {/*(<button onClick={() => setShowReplies(!showReplies)}>*/}
    //             {/*        +{repliesCount}*/}
    //             {/*    </button>*/}
    //             {/*)*/}
    //             {/*}*/}

    //             {/*{showReplies &&*/}
    //             {/*<div>*/}
    //             {/*    <h2>Sub comments.</h2>*/}
    //             {/*</div>*/}
    //             {/*}*/}
    //         </CommentBox>
    //     </CommentBase>
    // )
};
