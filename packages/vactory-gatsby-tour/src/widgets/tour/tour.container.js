import React, { useState, useEffect , useContext } from 'react'
import get from 'lodash.get';
import { Wysiwyg } from "vactory-gatsby-ui";
import loadable from '@loadable/component';
import { CookiesProvider } from 'react-cookie';
import { useCookies } from 'react-cookie';
import { PageContext } from 'vactory-gatsby-core/src/context';

//import Tour from 'reactour'
const TourLoadable = loadable.lib(() => import('reactour'))

export const TourContainer = ({ data }) => {
  const [isTourOpen, setIsTourOpen] = useState(true);
  const [cookies, setCookie] = useCookies(['visited']);
  const {nid} = useContext(PageContext)

  useEffect(() => {
    steps.map(step => {
      try {
        let element = document.querySelector(step.selector);
        if (element == null) {
          steps.splice(steps.indexOf(step), 1)
        }
      } catch (error) {
        steps.splice(steps.indexOf(step), 1)
      }

    })

    
  }, [])

  const steps = data.components.map(component => {
    return {
      selector: get(component, 'selector'),
      content: <Wysiwyg html={get(component, 'content.value.#text')} />,
    }
  });

  const tourOptions = data.extra_field.group_tours_options;

  Object.keys(tourOptions).map((option) => {
    if (typeof tourOptions[option] === "number") {
      tourOptions[option] = !!tourOptions[option];
    }
    if (option === "scrollDuration" || option === "startAt" || option === "rounded") {
      tourOptions[option] = parseInt(tourOptions[option]);
    }
  });

  const tourProps = {steps: steps, ...tourOptions};

  const isNidVisited = (nid) =>{
      if(cookies.visited){
        let array = cookies.visited.split(',')
        if(array.includes(nid+'')) return true
        else return false
      }
      else return false
  }

  return (
    <>
      <CookiesProvider>
        {
          !isNidVisited(nid)  && <TourLoadable fallback={''}>
            {({ default: Tour }) =>
              <Tour
                {...tourProps}
                isOpen={isTourOpen}
                onRequestClose={() => {
                  setIsTourOpen(false)
                  let items;
                  if(cookies.visited){
                      items = cookies.visited + ',' + nid
                  }else{
                    items = nid
                  }
                   
                  setCookie('visited',items)
                }  }
              />
            }
          </TourLoadable>
        }

      </CookiesProvider>
    </>
  )

}