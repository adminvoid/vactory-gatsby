import React, { useState, useContext } from "react";
import { useTranslation } from 'react-i18next'
import { CreateNewPost } from "./create-new";
import { AuthContext } from "vactory-gatsby-core"
import { AuthModalCta } from './auth-modal-cta'
import { useForm, Controller } from "react-hook-form";
import Select from 'react-select'

export const ForumHeading = ({ terms, value, handleChange, handleReset }) => {
	const { isAuthenticated } = useContext(AuthContext);
	const isLoggedin = isAuthenticated();
	const { t } = useTranslation();
	const [isOpen, setIsOpen] = useState(false)
	const [isAuthModalOpen, setisAuthModalOpen] = useState(false)
	const { register, handleSubmit, setValue, control } = useForm();

	const options = terms.map((term) => {
		return {
			label: term.name,
			value: term.id
		}
	  });	

	const openLayer = () => {
		if (isLoggedin) {
			setIsOpen(true)
		}
		else {
			setisAuthModalOpen(true)
		}
	}

	const closeLayer = () => {
		setIsOpen(false)
	}

	const closeAuthLayer = () => {
		setisAuthModalOpen(false)
	}

	React.useEffect(() => {
		setValue('forum_category', options.filter(option => value.includes(option.value)))
	}, []); //eslint-disable-line react-hooks/exhaustive-deps

	const onSubmit = (data) => {
		const cat = (
			data.forum_category && 
			Array.isArray(data.forum_category) && 
			data.forum_category.length > 0) ? data.forum_category.map(v => v.value) : 'all'
		handleChange(cat, data.forum_keyword);
	};
	
	const onReset = () => {
		setValue('forum_category', 'all');
		setValue('forum_keyword', '');
		handleReset();
	};


	return (
		<div className="pl-4 pr-6 pt-4 pb-4 border-b border-t border-gray-200 sm:pl-6 lg:pl-8 xl:pl-6 xl:pt-6 xl:border-t-0">
			{isLoggedin && <CreateNewPost isOpen={isOpen} closeLayer={closeLayer} terms={terms} />}
			{!isLoggedin && isAuthModalOpen && <AuthModalCta closeLayer={closeAuthLayer} />}
			<div className="flex items-center justify-between">
				<div className="">
					<button
						type="button"
						className="inline-flex items-center justify-center px-4 py-2 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500 xl:w-full"
						onClick={openLayer}
					>
						{t('Proposer un sujet')}
					</button>
				</div>
				<form className="flex sm:items-center space-x-4" onSubmit={handleSubmit(onSubmit)}>
					<div className="flex-1 min-w-0">
						<label htmlFor="search" className="sr-only">
							{t('Search')}
						</label>
						<div className="relative rounded-md shadow-sm">
							<div className="absolute inset-y-0 left-0 pl-3 flex items-center pointer-events-none">
								{/* Heroicon name: mail */}
								<svg
									className="h-5 w-5 text-gray-400"
									x-description="Heroicon name: solid/search"
									xmlns="http://www.w3.org/2000/svg"
									viewBox="0 0 20 20"
									fill="currentColor"
									aria-hidden="true"
								>
									<path
										fillRule="evenodd"
										d="M8 4a4 4 0 100 8 4 4 0 000-8zM2 8a6 6 0 1110.89 3.476l4.817 4.817a1 1 0 01-1.414 1.414l-4.816-4.816A6 6 0 012 8z"
										clipRule="evenodd"
									/>
								</svg>
							</div>
							<input
								type="search"
								name="forum_keyword"
								ref={register}
								id="search"
								className="focus:ring-pink-500 focus:border-pink-500 block w-full pl-10 py-3.5 sm:text-sm border-gray-300 rounded-md"
								placeholder="Search"
							/>
						</div>
					</div>

					<div className="w-60">
				<Controller
            as={
              <Select
			  isSearchable={false}
			  placeholder={t("Toutes les thématiques")}
			  isMulti={true}
			  styles={{ menuPortal: base => ({ ...base, zIndex: 9999 }) }}
			  menuPortalTarget={(typeof document !== "undefined" ? document.body : null)}
              />
            }
            isClearable
            options={options}
            onChange={([selected]) => {
              return selected
            }}
            control={control}
            name="forum_category"
          />

				</div>

					{/* <select // eslint-disable-line jsx-a11y/no-onchange
						defaultValue={value}
						id="forum-category"
						name="forum_category"
						ref={register}
						className="block pl-3 pr-10 py-2 text-base border-gray-300 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm rounded-md"
					>
						<option value="all">{t("Tous les thématiques")}</option>
						{terms.map((term) => (
							<option key={term.id} value={term.id}>
								{term.name}
							</option>
						))}
					</select> */}

					<div className="mt-3 rounded-md shadow sm:mt-0 sm:ml-3 sm:flex-shrink-0">
                        <button type="submit" className="w-full flex items-center justify-center px-5 py-3 border border-transparent text-base font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                            {t('Appliquer')}
                        </button>
                    </div>
                    <div className="mt-3 rounded-md shadow sm:mt-0 sm:ml-3 sm:flex-shrink-0">
                        <button 
                        type="button"
                        onClick={onReset}
                        className="w-full flex items-center justify-center px-5 py-3 border border-transparent text-base font-medium rounded-md text-white text-indigo-600 bg-white hover:bg-indigo-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                            {t('Reset')}
                        </button>
                    </div>
				</form>
			</div>
		</div>
	);
};
