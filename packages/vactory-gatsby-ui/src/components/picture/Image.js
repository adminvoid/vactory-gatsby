import React from "react"
import { AppSettings } from "vactory-gatsby-core"
import classNames from "classnames"

export const Image = (props) => {
    const backendURL = AppSettings.api.url;

    if (!props.file) {
        console.warn(`Image: missing file prop.`)
        return null;
    }

    const {
        file: {
            uri,
            meta: {
                alt: metaAlt,
            },
        },
        sizes = [],
        defaultSize = 'decoupled_image_320_320',
        alt,
        width,
        height,
        className = '',
        wrapperClassName = '',
        disableLazyLoad = false,
    } = props;

    const default_url = `${backendURL}sites/default/files/styles/${defaultSize}/public/${encodeURI(uri)}`;
    const finalAlt = alt || metaAlt || "";
    
    if (disableLazyLoad) {
        const sources = sizes.map((size, i) => {
            return (
                <source
                    key={i}
                    media={(size.media || "")}
                    srcSet={`${backendURL}sites/default/files/styles/${size.size}/public/${encodeURI(uri)}` || ""}
                />
            )
        }) || [];
    
        return (
            <picture>
                {sources}
                <img src={default_url} width={width} height={height} className={className}
                    alt={finalAlt} />
            </picture>
        )
    }
    else {
        const sources = sizes.map((size, i) => {
            return (
                <source
                    key={i}
                    media={(size.media || "")}
                    data-srcset={`${backendURL}sites/default/files/styles/${size.size}/public/${encodeURI(uri)}` || ""}
                />
            )
        }) || [];
    
        return (
            <picture className={wrapperClassName}>
                {sources}
                <img data-src={default_url} src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" width={width} height={height} className={classNames("lazyload", className)}
                    alt={finalAlt} />
            </picture>
        )
    }
};

