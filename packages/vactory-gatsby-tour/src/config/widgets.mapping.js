import {
  TourContainer,
} from "vactory-gatsby-tour";

const widgets = {
  "vactory_tour:tour": TourContainer,
};

export default widgets;
