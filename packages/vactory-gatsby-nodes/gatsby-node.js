const api = require('vactory-gatsby-api');
const path = require("path");
const chalk = require("chalk");
const fse = require("fs-extra");
const _ = require('lodash');
const viewsConfigurationFile = `${__dirname}/.tmp/viewsConfig.json`;
const prettyURLPathPatternsFile = `${__dirname}/.tmp/prettyURLPathPatterns.json`;

exports.onPreBootstrap = async () => {
    try {
        await fse.ensureFile(viewsConfigurationFile);
        await fse.writeJson(viewsConfigurationFile, {
            views: [],
        })
    } catch (err) {
        console.error(err)
    }

    try {
        await fse.ensureFile(prettyURLPathPatternsFile);
        await fse.writeJson(prettyURLPathPatternsFile, {
            patterns: [],
        })
    } catch (err) {
        console.error(err)
    }
};

exports.createPages = async ({store, actions: {createPage}}, {
    title,
    resource,
    params,
    template,
    amp = {
        enabled: false,
        template: null
    },
    nodeProcessor = null,
    addContext = null,
}) => {
    console.log(chalk.green("[\u2713] " + title));

    // Create a mapping file in order to get alias by view_id field.
    // search > /fr/recherche + /ar/search
    let viewsData = await fse.readJson(viewsConfigurationFile);
    const view_id = _.get(params, 'filter.field_view_id');

    // Pretty URL patterns
    let urlPatternsData = await fse.readJson(prettyURLPathPatternsFile);

    // Preview config.
    const previewEnabled = true;
    const previewTemplate = path.resolve(__dirname, `src/templates/preview.js`);

    // @todo: benchmark this, maybe add cache to it.
    const {breadcrumbs} = await fse.readJson(`${__dirname}/../vactory-gatsby-core/.tmp/breadcrumbs.json`);

    let response = [];
    try {
        response = await api.getAll(resource, params);
    } catch (error) {
        console.error(error);
        return;
    }

    const flattenNodes = [].concat(...response);

    // Merge nodes with translations.
    let mergedNodes = [];
    flattenNodes.forEach(node => {
        if (typeof mergedNodes[node.drupal_internal__nid] === "undefined") {
            mergedNodes[node.drupal_internal__nid] = []
        }

        mergedNodes[node.drupal_internal__nid].push(node)
    });

    // Clean up the array.
    // we end up with empty array items since we relay on "drupal_internal__nid"
    // as an index for the array.
    const nodes = mergedNodes.filter(function (el) {
        return el != null
    });

    for (let i18nNodes of nodes) {
        // This is used  mostly by the switch language dropdown.
        // It needs to know the URL to the translated node.
        const pageInfo = i18nNodes.map(nodeLocale => {
            return {
                locale: nodeLocale.path.langcode,
                path: nodeLocale.path.alias,
                title: nodeLocale.title,
                type: nodeLocale.type,
            }
        });

        for (let node of i18nNodes) {
            let extraContext = {};
            const nodeBreadcrumb = breadcrumbs.find(link => link.path === node.path.alias);
            const nodeBreadcrumbItems = nodeBreadcrumb ? nodeBreadcrumb.items : [];

            // Override langcode.
            node.langcode = node.path.langcode;

            // Hook into nodes.
            if (nodeProcessor) {
                node = await nodeProcessor(node);
            }

            // Log.
            console.log(
                `${chalk.blue("[\u25E6]")} ${chalk.blue(
                    node.path.alias,
                )}`,
            );

            // Hooks into context.
            if (addContext) {
                extraContext = await addContext(node);
            }

            if (view_id) {
                viewsData.views.push({
                    id: view_id,
                    alias: node.path.alias,
                    langcode: node.langcode,
                })
            }

            let matchPath = undefined;
            if (node.node_settings) {
                let node_settings = null;
                try {
                    node_settings = JSON.parse(node.node_settings)
                  } catch (e) {
                    console.error(`Node settings parsing error at ${node.path.alias}:`, e);
                  }

                  extraContext.hasMatchPath = false;
                  extraContext.internalMatchPath = null;
                  if (
                    typeof node_settings.url_params !== 'undefined' &&
                    typeof node_settings.url_params === 'object' &&
                    Array.isArray(node_settings.url_params)
                    ) {
                        let node_url_params = node_settings.url_params.map(param => ':' + param)
                        node_url_params = node_url_params.join('/')
                        matchPath = `${node.path.alias}/${node_url_params}/`
                        extraContext.internalMatchPath = matchPath;
                        const _url_from = `${node.path.alias}${"/(.+?)".repeat(node_settings.url_params.length)}`.replace(/^\/+/g, '') + '/'
                        const _url_to = `${matchPath}/index.html`.replace(/^\/+/g, '')
                        urlPatternsData.patterns.push({
                            from: _url_from,
                            to: _url_to,
                            rule: `RewriteRule ^${_url_from}$ ${_url_to} [L]`
                        })
                         // Log filters.
                        console.log(
                            `${chalk.blue("[\u25E6]")} > ${chalk.italic(
                                matchPath,
                            )}`,
                        );
                }
            }

            // VCC.
            if (JSON.stringify(params).indexOf('vcc_normalized') > -1 ) {
                const vcc_ids = node.vcc_normalized.ids || []
                let vccContext = {
                    config: node.vcc_normalized,
                    nodes: []
                };

                if (vcc_ids.length > 0) {
                    let nidsFilter = {
                        "filter[ids][condition][path]": "drupal_internal__nid",
                        "filter[ids][condition][operator]": "IN",
                        "filter[ids][condition][value]": []
                    };
                
                    vcc_ids.forEach(id => {
                        nidsFilter['filter[ids][condition][value]'].push(id);
                    });

                    const VCC_params = {
                        ...nidsFilter,
                        fields: params.fields,
                        include: params.include,
                        page: {
                            limit: vccContext.config.limit
                        }
                    };

                    const mapOrder = (array, order, key) => {
                        array.sort( function (a, b) {
                            var A = a[key], B = b[key];
                    
                            if (order.indexOf(A) > order.indexOf(B)) {
                                return 1;
                            } else {
                                return -1;
                            }
                    
                        });
                    
                        return array;
                    };

                    const VCC_response = await api.getResponse(resource, VCC_params, node.langcode);
                    vccContext.nodes = mapOrder(VCC_response.data, vcc_ids, 'drupal_internal__nid');
                    extraContext._VCC = vccContext
                }
            }

            createPage({
                path: node.path.alias,
                component: template,
                context: {
                    node: node,
                    hasAMP: false,
                    breadcrumb: nodeBreadcrumbItems,
                    pageInfo,
                    ...extraContext,
                    hasMatchPath: matchPath ? true : false
                },
            });

            // @todo: test amp & preview ?
            if (matchPath) {
                createPage({
                    path: matchPath,
                    matchPath: matchPath,
                    component: template,
                    context: {
                        node: node,
                        hasAMP: false,
                        breadcrumb: nodeBreadcrumbItems,
                        pageInfo,
                        ...extraContext,
                        isMatchPath: true
                    },
                });
            }

            // AMP.
            if (amp.enabled) {
                createPage({
                    path: path.join(node.path.alias, "/amp/"),
                    component: amp.template,
                    context: {
                        node: node,
                        hasAMP: true,
                        breadcrumb: nodeBreadcrumbItems,
                        pageInfo,
                        ...extraContext,
                    },
                });
            }

            // Preview.
            if (previewEnabled) {
                // Get package name.
                const relativePath = path.relative(path.join(__dirname, '..'), template);
                const parts = relativePath.split(path.sep);
                let packageName = parts[0];
                const previewFilePath = path.format({
                    dir: `${path.resolve(path.join(__dirname, '..'))}/${packageName}/src/components`,
                    base: 'preview.container.js'
                });

                // If no preview file is found.
                // Fallback to default preview file.
                const previewFileExist = await fse.exists(previewFilePath);
                if (!previewFileExist) {
                    packageName = 'vactory-gatsby-nodes';
                    throw new Error(
                        `[${packageName}] Preview file not found at ${previewFilePath}`
                    )
                }

                createPage({
                    path: path.join(node.path.alias, "/__preview/"),
                    component: previewTemplate,
                    context: {
                        node: node,
                        pageInfo,
                        ...extraContext,
                        previewConfig: {
                            node_type: resource.split("/").pop(),
                            resource,
                            params,
                            packageName,
                        }
                    },
                });
            }
        }

    }

    // Save views config.
    await fse.writeJson(viewsConfigurationFile, viewsData)
    // Save pretty URL patterns.
    try {
        await fse.writeJson(prettyURLPathPatternsFile, urlPatternsData)
    } catch (err) {
        console.error(err)
    }
};

exports.onPostBuild = async ({ store }) => {
    console.log(chalk.green("[\u2713] Build .htaccess Pretty URL's rules."));

    const { program } = store.getState()
    const getPath = (htPath, program) => path.join(program.directory, htPath, '.htaccess')
    const contentReadFile = pathToFile => fse.readFileSync(pathToFile, 'utf8')
    const setCustom = (content, custom) => custom
    ? content.replace('### CUSTOM-RULES ###', custom)
    : content
    const htPath = getPath('public', program)
    let urlPatternsData = await fse.readJson(prettyURLPathPatternsFile);
    let content = contentReadFile(htPath)

    if (urlPatternsData.patterns.length <= 0) {
         // Log.
         console.log(
            `${chalk.blue("[\u25E6]")} ${chalk.blue('No rules to generate')}`,
        );
        return;
    }

    const rules = urlPatternsData.patterns.map(pattern => pattern.rule)
    const rewrtiteRules = rules.join("\n");

    content = setCustom(content, `
    <IfModule mod_rewrite.c>
        RewriteEngine On
        RewriteCond %{REQUEST_FILENAME} !-f
        RewriteCond %{REQUEST_FILENAME} !-d
        ${rewrtiteRules}
    </IfModule>
    `)

    try {
        await fse.ensureFile(htPath)
      } catch (e) {
        console.error('onPostBuild error #hq0Kxa', e.getMessage())
        return;
    }

    await fse.writeFile(htPath, content)
};