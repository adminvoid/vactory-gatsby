import {AppSettings, ImageStyles} from "vactory-gatsby-core"

export const useImageStyle = (fakeImageStyleName) => {
	const backendURL = AppSettings.api.url;

	const imageStyle = ImageStyles.find(style => style.name === fakeImageStyleName);

	if (!imageStyle) {
		console.warn(`ImageStyle: ${fakeImageStyleName} is not found.`)
		return (imageFile) => {
			if (!imageFile) {
				console.warn(`Missing Image File`, imageFile)
				return '';
			}
			const {uri} = imageFile;
			return `${backendURL}sites/default/files/${encodeURI(uri)}`
		}
	}

	const imageStyleName = `decoupled_image_${imageStyle.width}_${imageStyle.height}`;

	return (imageFile) => {
		if (!imageFile) {
			console.warn(`Missing Image File`, imageFile)
			return '';
		}

		const {uri} = imageFile;
		return `${backendURL}sites/default/files/styles/${imageStyleName}/public/${encodeURI(uri)}`
	}
}