import {MailchimpWidgetContainer} from 'vactory-gatsby-mailchimp'

export const widgets = {
  'vactory_mailchimp:mailchimp_simple': MailchimpWidgetContainer,
};
