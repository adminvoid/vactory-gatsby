import React from "react"
import {Link as GatsbyLink} from "gatsby"
import clsx from "clsx";


export const Link = props => {
    let { to, className, attributes, ...newProps } = props;
    to ??= props?.href;
    const internal = /^\/(?!\/)/.test(to)
    const isFile = /^\/sites\/default\/(?!\/)/.test(to)

    newProps.className = clsx(className, attributes?.class)
    newProps.target = attributes?.target || props?.target || null;
    newProps.rel = attributes?.rel || props?.rel || null;
    newProps.id = attributes?.id || props?.id || null;

    if (internal && !isFile) {
        return <GatsbyLink to={to} {...newProps}>{props.children}</GatsbyLink>
    }

    if (isFile) {
        return (
            <a {...newProps} href={`/backend${to}`}>
                {props.children}
            </a>
        )
    }


    return (
        <a {...newProps} href={to}>
            {props.children}
        </a>
    )
};
