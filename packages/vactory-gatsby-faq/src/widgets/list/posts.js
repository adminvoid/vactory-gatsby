import React from "react";
import { Icon } from "vactory-gatsby-ui";
import { Accordion } from "mre";

export const PostsPage = ({ posts }) => {
	return (
		<>
			<h2 className="sr-only">Page content</h2>
			{posts.map((post) => {
				return (
					<article key={post.key} className="mb-6 md:mb-12">
						<h3 className="text-xl md:text-3xl tracking-tight font-bold leading-tight md:leading-normal section-heading mb-6 md:mb-12">
							{post.title}
							<Icon
								name="awb-symbol"
								width="37"
								height="10"
								className="w-9 text-primary my-4"
							/>
						</h3>

						<Accordion
							items={post.questions}
							keys={{
								titleKey: "question",
								descriptionKey: "answer",
							}}
						/>
					</article>
				);
			})}
		</>
	);
};
