const api = require('vactory-gatsby-api');
const fse = require('fs-extra');
const chalk = require('chalk');
const path = require("path");
const publicPath = `./public`;
const output = "/.htaccess";

exports.onPreBootstrap = async () => {
    console.log(chalk.bold.white.bgBlue(`[\u2713] Download redirections.`));

    const response = await api.getRest("vapi/redirections", {}, false);

    //save the redirections in .tmp file
    const configurationFile = `${__dirname}/.tmp/redirections.json`;
    try {
        await fse.ensureFile(configurationFile);
        await fse.writeJson(configurationFile, response.data.redirections);
    } catch (err) {
        console.error(err)
    }
}

exports.onPostBuild = async ({store}) => {
    const redirectionsFile = `${__dirname}/.tmp/redirections.json`;
    const htaccessFile = path.join(publicPath, output);

    console.log(chalk.green("[\u2713] " + redirectionsFile));
    console.log(chalk.green("[\u2713] " + htaccessFile));

    const { program } = store.getState()
    const getPath = (htPath, program) => path.join(program.directory, htPath, '.htaccess')
    const contentReadFile = pathToFile => fse.readFileSync(pathToFile, 'utf8')
    const setCustom = (content, custom) => custom
    ? content.replace('### REDIRECTION-RULES ###', custom)
    : content
    const htPath = getPath('public', program)
    let content = contentReadFile(htPath)
    const redirections = await fse.readJson(redirectionsFile);

    const httpStatus = {
        301: 'permanent',
        302: 'temp',
        303: 'seeother',
        410: 'gone'
    };

    const findHttpStatus = (status) => {
        if (!(status in httpStatus)) {
            return status
        }

        return httpStatus[status];
    };

    let redirectsRules = '';

    redirections.forEach(item => {
        const status = findHttpStatus(item.status)
        let oldURL = item.old
        let newURL = item.new

        if (status === 'gone') {
            newURL = ''
        }

        const rule = `Redirect ${findHttpStatus(item.status)} ${oldURL} ${newURL}` + "\n";
        redirectsRules += rule
    })
    content = setCustom(content, `
    <IfModule mod_rewrite.c>
        RewriteEngine On
        ${redirectsRules}
    </IfModule>
    `)

    try {
        await fse.ensureFile(htPath)
      } catch (e) {
        console.error('onPostBuild error #hq0Kxa', e.getMessage())
        return;
    }

    await fse.writeFile(htPath, content)

   /* try {

        // Getting content from Json file in /.tmp
        const redirectionContent = await fse.readJson(redirectionsFile);
        console.log(redirectionContent)
        if (redirectionContent == null) {
            return;
        }
        else {
            myRules = []
            myRules += '<IfModule mod_rewrite.c>\n'
            // Rules that going to be written 
            let red = "Redirect"
            let Perma = red + " permanent ";
            let temp = red + " temp "
            let SO = red + " see other "
            let gone = red + "Match gone"

            // converting Rules from Json file to string Array to be inserted in ifmodule tag
            for (var i = 0; i < redirectionContent.length; i++) {
                let ol = "\"" + redirectionContent[i]["old"] + "\""
                let nw = "\"" + redirectionContent[i]["new"] + "\""
                switch (redirectionContent[i]["status"]) {
                    case "301":
                        var fullRule = '\t' + Perma + ol + ' ' + nw + '\n';
                        break;
                    case "302":
                        var fullRule = '\t' + temp + ol + ' ' + nw + '\n';
                        break;
                    case "303":
                        var fullRule = '\t' + SO + ol + ' ' + nw + '\n'
                        break;
                    case "410":
                        var fullRule = '\t' + "RewriteEngine ON\n"
                            + '\t' + gone + ' ' + ol + ' ' + nw + '\n'

                        break;
                    default:
                        var fullRule = '\t' + red + ' ' + ol + ' ' + nw + '\n';
                }
                myRules += fullRule
            }
            myRules = myRules + '</IfModule>'

            console.log(myRules)

            content = setCustom(content, myRules)

            try {
                await fse.ensureFile(htPath)
              } catch (e) {
                console.error('onPostBuild error #hq0Kxa', e.getMessage())
                return;
            }

            await fse.writeFile(htPath, content)

        }

    } catch (err) {
        console.error(err)
    }*/
}