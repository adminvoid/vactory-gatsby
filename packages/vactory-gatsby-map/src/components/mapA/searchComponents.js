import React from "react";
import { useTranslation } from "react-i18next";
import default_image from "./images/building.jpg";
import { LoadingOverlay } from "vactory-gatsby-ui";
import placeholder_image from "./images/placeholder.jpg";

export const SearchResult = ({
	image = "default",
	name,
	locality,
	addressLine1,
	addressLine2,
	phone,
	phone2,
	fax,
	...rest
}) => {
	const { t } = useTranslation();
	let image_url, image_style;

	if (image === "default") {
		image_url = default_image;
		let hueHash =
			(name + " " + addressLine1)
				.split("")
				.reduce((acc, cur) => acc + cur.charCodeAt(0), 0) % 360;
		image_style = {
			filter: `sepia(50%) hue-rotate(${hueHash}deg)`,
		};
	} else image_url = image;

	return (
		<div className="card flex items-center max-w-sm" {...rest}>
			<div className="mr-2.5 flex-shrink-0">
				<img
					className="object-cover"
					src={image_url}
					alt={name}
					width={78}
					height={78}
					style={image_style}
				/>
			</div>
			<div>
				<h5 className="font-medium text-sm">{name}</h5>
				<dl className="text-gray-400 text-xs">
					{locality && (
						<div>
							<dt className="sr-only">Name</dt>
							<dd>{locality}</dd>
						</div>
					)}
					{addressLine1 && (
						<div>
							<dt className="sr-only">address Line 1</dt>
							<dd>{addressLine1} </dd>
						</div>
					)}
					{addressLine2 && (
						<div>
							<dt className="sr-only">address Line 2</dt>
							<dd>{addressLine2} </dd>
						</div>
					)}
					{phone && (
						<div className="flex">
							<dt className="font-bold ltr:mr-1 rtl:ml-1">
								{t("Phone")}:
							</dt>
							<dd>{phone}</dd>
						</div>
					)}
					{phone2 && (
						<div className="flex">
							<dt className="font-bold ltr:mr-1 rtl:ml-1">
								{t("Phone")}:
							</dt>
							<dd>{phone2}</dd>
						</div>
					)}
					{fax && (
						<div className="flex">
							<dt className="font-bold ltr:mr-1 rtl:ml-1">
								{t("Fax")}:
							</dt>
							<dd>{fax}</dd>
						</div>
					)}
				</dl>
			</div>
		</div>
	);
};

export const LoadingPlaceholder = () => {
	return (
		<LoadingOverlay
			active={true}
			text="taking care of stuff.."
			styles={{
				wrapper: (base) => ({
					...base,
					height: "100%",
				}),
				overlay: (base) => ({
					...base,
					background: `linear-gradient(rgba(0,0,0,.4) 100%, #000), rgba(0,0,0, .7) url(${placeholder_image}) center / cover`,
				}),
			}}
		/>
	);
};
