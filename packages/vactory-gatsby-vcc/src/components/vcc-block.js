import React from 'react'
import {useTranslation} from "react-i18next"
import { Transition } from "@headlessui/react";
import {Link, CardContentLoader} from "vactory-gatsby-ui";

export const VCCBlock = ({
	title, 
	linkLabel, 
	renderNode, 
	config, 
	normalizer, 
	wrapperClass = 'flex flex-wrap', 
	itemClass = "w-full sm:w-1/2 md:w-1/3 px-1 sm:px-2: md:px-3"
}) => {
    const {t} = useTranslation();
    const posts = normalizer(config?.nodes || []);
    const moreLink = config?.config?.link_more || null;
    const limit = config?.config?.limit || 3;
    let internalTitle = title ? title : t('Articles similaires');
    let internalLinkLabel = linkLabel ? linkLabel : t('Voir plus');

    if (posts.length <= 0) { return <></> }

    return (
		<div className="bg-gray-eee">
			<div className="container">
                <div className="py-12">
				<div className="text-center mb-12">
					<h2 className="text-xl md:text-3xl tracking-tight font-bold leading-tight md:leading-normal section-heading text-black">
						{internalTitle}
					</h2>
					{/* {raw_description.length > 0 && (
						<div className="my-3 max-w-2xl mx-auto text-xl text-gray-500 dark:text-gray-300 sm:mt-4">
							{description}
						</div>
					)} */}
				</div>

				{(posts && (
					<div className={wrapperClass}>
						{posts.map((node) => (
                            <Transition
                                key={node.id}
                                show={true}
                                enter="transform ease-out duration-300 transition"
                                enterFrom="scale-90 opacity-0"
                                enterTo="scale-100 opacity-100"
                                leave="transform ease-out duration-300 transition"
                                leaveFrom="scale-100 opacity-100"
                                leaveTo="scale-90 opacity-0"
                                className={itemClass}
                            >
                                {renderNode(node)}
                            </Transition>
						))}
					</div>
				)) || (
					<div className={wrapperClass}>
						{Array(limit).fill().map((e, i) => (
                            <div key={i} className={itemClass}>
                                <CardContentLoader />
                            </div>
                        ))}
					</div>
				)}

				{moreLink && posts.length >= limit  && (
                    <div className="flex justify-center mt-12">
					<Link
						href={moreLink}
						className="inline-flex items-center px-6 py-3 border border-black relative group"
					>
						<div className="absolute inset-y-0 ltr:right-0 rtl:left-0 w-full group-hover:w-0 transition bg-black" />
						<span className="relative z-1 text-white group-hover:text-black text-base font-medium">
							{internalLinkLabel || "See More"}
						</span>
					</Link>
				</div>
					
				)}
                </div>
			</div>
		</div>
	);
};
