import React from "react"
import clsx from "clsx";
import { Link } from 'vactory-gatsby-ui'
import { useTranslation } from "react-i18next"

import * as dayjs from "dayjs"
import 'dayjs/locale/fr';
import 'dayjs/locale/ar';

export const ForumCard = ({ item }) => {
    const { i18n, t } = useTranslation();
    const currentLanguage = i18n.language;
    const dayjsLocale = currentLanguage === 'ar' ? 'ar-ma' : currentLanguage
    dayjs.locale(dayjsLocale)

    return <Link to={item.url} className="block hover:bg-gray-50">
        <div className="px-4 py-4 flex items-center sm:px-6">
            <div className="min-w-0 flex-1 sm:flex sm:items-center sm:justify-between">
                <div>
                    <div>
                        <div className="flex text-sm font-medium text-indigo-600 truncate items-center">
                            <span
                                className={clsx("ltr:mr-1 rtl:ml-1 h-4 w-4 rounded-full flex items-center justify-center", item.status === "open" ? "bg-green-100" : "bg-gray-100")}
                                aria-hidden="true"
                            >
                                <span className={clsx("h-2 w-2 rounded-full", item.status === "open" ? "bg-green-400 " : "bg-gray-400")} />
                            </span>
                            <p>{item.title}</p>
                            <p className="ml-1 font-normal text-gray-500">
                                {'dans'} {item.section.join(', ')}
                            </p>

                        </div>
                        <p className="mt-1 text-sm text-gray-600 line-clamp-2">
                            {item.excerpt}
                        </p>
                    </div>

                    <div className="mt-2 flex">
                        <div className="flex items-center text-sm text-gray-500 ltr:mr-2 rtl:ml-2">
                            {/* Heroicon name: solid/user */}
                            <svg className="h-5 w-5 text-gray-400 ltr:mr-1 rtl:ml-1" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M16 7a4 4 0 11-8 0 4 4 0 018 0zM12 14a7 7 0 00-7 7h14a7 7 0 00-7-7z" />
                            </svg>
                            <p className="whitespace-nowrap">

                                {item.author}
                            </p>
                        </div>
                        <div className="flex items-center text-sm text-gray-500 ltr:mr-2 rtl:ml-2">
                            {/* Heroicon name: solid/clock */}
                            <svg className="h-5 w-5 text-gray-400 ltr:mr-1 rtl:ml-1" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M12 8v4l3 3m6-3a9 9 0 11-18 0 9 9 0 0118 0z" />
                            </svg>
                            <p>
                                <time dateTime={dayjs(item.date).format('DD/MM/YYYY')}>{dayjs(item.date).format('ddd, DD/MM/YYYY')}</time>
                            </p>
                        </div>
                        <div className="flex items-center text-sm text-gray-500">
                            {/* Heroicon name: solid/chat-alt-2 */}
                            <svg className="h-5 w-5 text-gray-400 ltr:mr-1 rtl:ml-1" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M17 8h2a2 2 0 012 2v6a2 2 0 01-2 2h-2v4l-4-4H9a1.994 1.994 0 01-1.414-.586m0 0L11 14h4a2 2 0 002-2V6a2 2 0 00-2-2H5a2 2 0 00-2 2v6a2 2 0 002 2h2v4l.586-.586z" />
                            </svg>
                            <p>
                                {item.contributions} {t('réponses')}
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div className="ml-5 flex-shrink-0">

                <div className="relative text-sm text-gray-500 font-small flex items-center">
                    {/* <span>
            {t('Voir la discussion')}
          </span> */}
                    <svg className="h-5 w-5 text-gray-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                        <path fillRule="evenodd" d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z" clipRule="evenodd" />
                    </svg>
                </div>


            </div>
        </div>
    </Link>
}
