import get from 'lodash.get';

export const normalizeNodes = (nodes) => {
    return nodes.map(post => ({
        id: post.drupal_internal__nid,
        title: post.title,
        url: get(post, 'path.alias', '#.'),
        excerpt: get(post, 'excerpt.value', null),
        views: get(post, 'views_count', 0),
        status: get(post, 'status', 'closed'),
        contributions: get(post, 'internal_comment.contributions', 0),
        last_contribution: get(post, 'internal_comment.last_contribution', null),
        section: [get(post, 'category.name', null)],
        author: get(post, 'internal_user.field_forum_editeur.full_name', null),
        date: get(post, 'field_vactory_date', 0),
    }));
};

export const normalizeDate = (lang, date) => {
  return null;
};

export const normalizeDFNodes = (nodes) => {
    return nodes.map(post => ({
        id: post.key,
        title: post.title,
        url: get(post, 'url', null),
        excerpt: get(post, 'excerpt', null),
        views: get(post, 'views', 0),
        status: get(post, 'status', 'closed'),
        contributions: get(post, 'contributions', 0),
        last_contribution: get(post, 'last_contribution', null),
        section: get(post, 'section', []),
        author: get(post, 'author', null),
        date: get(post, 'date', 0),
    }));
};

export const normalizeNode = (post) => {
    return {
      id: post.id,
      nid: post.drupal_internal__nid,
      title: post.title,
      url: get(post, 'path.alias', '#.'),
      body: get(post, 'body.processed', null),
      comment: get(post, 'comment.last_comment_name', null),
      forum_expert: get(post, 'internal_user.field_forum_expert', null),
      excerpt: get(post, 'field_vactory_excerpt.processed', null),
      category: get(post, 'field_vactory_taxonomy_1.name', null),
    }
  }
