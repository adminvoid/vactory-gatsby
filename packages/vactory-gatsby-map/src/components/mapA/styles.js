import styled from "styled-components";

export const MapContainer = styled.div`
  padding-top: 30px;
  padding-bottom: 30px;
  background-color: #FFF;
  
  h2 {
    font-size: 15px;
    line-height: 22px;
    color: #707070;
    margin-bottom: 30px;
    .title-icon {
      margin-right: 32px;
      width: 68px;
      height: 68px;
      display: flex;
      justify-content: center;
      align-items: center;
      position: relative;
      
 
      &:before {
        content: '';
        display: block;
        position: absolute;
        width: 100%;
        height: 100%;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%) rotate(45deg);
        z-index: 1;
        background-color: red;
      }
      svg {
        color: red;
        position: absolute;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);
        z-index: 2;
        width: 30px;
      }
    }
  }
  .map-input-wrapper {
    position: relative;
    z-index: 0;
    height: 500px;
    margin: 0 auto;
    box-shadow: 0px 10px 16px 0px #cacaca;
    
    .inner {
      height: 100%;
      position: static;
    }
  }

  #map-container-a {
    height: 100%;
  }

`;

