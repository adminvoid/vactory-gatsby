import React from 'react';
import { I18nextProvider } from 'react-i18next';
import { PageContext } from './context';
import i18nInstance from './i18n/i18nInstance';
import {getLanguageByPathname} from './utils/language'


export const wrapPageElement = ({element, props}, pluginOptions) => {
    const pageContextData = {
        params: props?.params || {},
        hasMatchPath: props?.pageContext?.hasMatchPath || false,
        isMatchPath: props?.pageContext?.isMatchPath || false,
        internalMatchPath: props?.pageContext?.internalMatchPath || null,
        pathName: props?.location?.pathname || '',
        nid: props?.pageContext?.node?.drupal_internal__nid || false
    }

    const lngCode = getLanguageByPathname(props?.location?.pathname || '');
    const i18nClone = i18nInstance.cloneInstance({
        lng: lngCode,
        fallbackLng: lngCode
    });

    // console.log(pageContextData)
    return (
        <I18nextProvider i18n={i18nClone}>
            <PageContext.Provider value={pageContextData}>
                {element}
            </PageContext.Provider>
        </I18nextProvider>
    )
};