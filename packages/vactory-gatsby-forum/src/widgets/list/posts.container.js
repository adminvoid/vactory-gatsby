import React, { useState, useEffect, useRef, useContext } from "react";
import { useTranslation } from "react-i18next";
import Api from "vactory-gatsby-api";
import {
	postsQueryParams,
	normalizeNodes,
	normalizeDFNodes,
	PostsPage,
} from "vactory-gatsby-forum";
import { Link, LoadingOverlay, Pagination } from "vactory-gatsby-ui";
import { ForumHeading } from "../../components/forumHeading";
import {PageContext, findTermsFromPath, findTermsSlugFromIds} from 'vactory-gatsby-core'

const PostsContainer = ({ pageCount, nodes, components, categories }) => {
	const { t, i18n } = useTranslation();
	const currentLanguage = i18n.language;
	const {params, internalMatchPath, isMatchPath} = useContext(PageContext);
	const normalizedNodes = normalizeDFNodes(nodes);
	const isFirstRun = useRef(isMatchPath ? false : true);
	const [posts, setPosts] = useState(isMatchPath ? [] : normalizedNodes);
	const [setectedFilter, setSelectedFilter] = useState({
		terms: isMatchPath ? findTermsFromPath(params.theme, categories, 'all') : 'all',
		keyword: ''
	  });
	const [isLoading, setIsLoading] = useState(false);
	const [pager, setPager] = useState(1);
	const [count, setCount] = useState(isMatchPath ? 0 : pageCount);

	const handleChange = (term_tid, _keyword) => {
		setSelectedFilter({
			terms: term_tid,
			keyword: _keyword
		})
		setPager(1);
    	updateURLHistory(term_tid)
	};

	const handlePaginationChange = (selected) => {
		setPager(selected);
	};

	const updateURLHistory = (section) => {
        // Update URL history.
        if (internalMatchPath) {
            let url = internalMatchPath;
			url = url.replace(':theme', findTermsSlugFromIds(section, categories, 'all' ) || 'all')
            window.history.pushState(null, null, url)
        }
    }

	const handleReset = () => {
		setPager(1);
		setSelectedFilter({
		  terms: 'all',
		  keyword: ''
		})
		updateURLHistory('all');
	  }

	useEffect(() => {
		if (isFirstRun.current) {
			isFirstRun.current = false;
			return;
		}

		function fetchData() {
			let categoryFilter = {
				"filter[category][condition][path]":
					"field_vactory_taxonomy_1.drupal_internal__tid",
				"filter[category][condition][operator]": "IN",
				"filter[category][condition][value]": setectedFilter.terms,
			};

			let searchFilter = {
				"filter[label][condition][path]": "title",
				"filter[label][condition][operator]": "CONTAINS",
				"filter[label][condition][value]": setectedFilter.keyword,
			}

			if (setectedFilter.terms === "all") {
				categoryFilter = {};
			}

			const requestParams = {
				...postsQueryParams,
				page: {
					limit: postsQueryParams.page.limit,
					offset: (pager - 1) * postsQueryParams.page.limit,
				},
				...categoryFilter,
				...searchFilter,
			};

			setIsLoading(true);
			Api.getResponse(
				"node/vactory_forum",
				requestParams,
				currentLanguage
			)
				.then((res) => {
					const normalizedNodes = normalizeNodes(res.data);
					const total = res.meta.count;
					setPosts(normalizedNodes);
					setCount(total);
					setIsLoading(false);
				})
				.catch((err) => {
					setIsLoading(false);
					console.log(err);
				});
		}
		fetchData();
	}, [setectedFilter, currentLanguage, pager]);

	return (
		<>
			<div className="overflow-y-auto">
				<div className="relative min-h-screen flex flex-col">
					{/* 3 column wrapper */}
					<div className="flex-grow w-full max-w-7xl mx-auto xl:px-8 lg:flex">
						{/* Left sidebar & main wrapper */}
						<div className="flex-1 min-w-0 bg-white xl:flex">
							{/* Projects List */}
							<div className="bg-white lg:min-w-0 lg:flex-1">
								<ForumHeading
									title={components.title}
									terms={categories}
									value={setectedFilter.terms}
									handleChange={handleChange}
									handleReset={handleReset}
									keywordValue={setectedFilter.keyword}
								/>

								<LoadingOverlay active={isLoading}>
									{posts.length > 0 && <PostsPage posts={posts} />}
									{!isLoading && posts.length <= 0 && (
										<p>{t("Aucun résultat trouvé")}</p>
									)}
								</LoadingOverlay>

								{count > postsQueryParams.page.limit && (
									<Pagination
										total={count}
										defaultPageSize={postsQueryParams.page.limit}
										pageSize={postsQueryParams.page.limit}
										current={pager}
										onChange={handlePaginationChange}
									/>
								)}

								{components.show_link && components.link && (
									<Link to={components.link}>
										{components.link_label}
									</Link>
								)}
							</div>
						</div>
						{/* <ActivityFeed /> */}
					</div>
				</div>
			</div>
		</>
	);
};

export default PostsContainer;
