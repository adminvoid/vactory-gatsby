import React from "react";

export const ActivityFeed = (props) => {
	return (
		<div className="bg-gray-50 lg:flex-shrink-0 lg:border-l lg:border-gray-200">
			<div className="pl-6 pr-6 lg:w-80">
				<div className="pt-6 pb-2">
					<h2 className="text-sm font-semibold">Activity</h2>
				</div>
				<div>
					<ul className="divide-y divide-gray-200" x-max={1}>
						<li className="py-4">
							<div className="flex space-x-3">
								<img
									className="h-6 w-6 rounded-full"
									src="https://images.unsplash.com/photo-1517365830460-955ce3ccd263?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=256&h=256&q=80"
									alt="al"
								/>
								<div className="flex-1 space-y-1">
									<div className="flex items-center justify-between">
										<h3 className="text-sm font-medium">
											You
										</h3>
										<p className="text-sm text-gray-500">
											1h
										</p>
									</div>
									<p className="text-sm text-gray-500">
										Deployed Workcation (2d89f0c8 in master)
										to production
									</p>
								</div>
							</div>
						</li>
						<li className="py-4">
							<div className="flex space-x-3">
								<img
									className="h-6 w-6 rounded-full"
									src="https://images.unsplash.com/photo-1517365830460-955ce3ccd263?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=256&h=256&q=80"
									alt="a"
								/>
								<div className="flex-1 space-y-1">
									<div className="flex items-center justify-between">
										<h3 className="text-sm font-medium">
											You
										</h3>
										<p className="text-sm text-gray-500">
											3h
										</p>
									</div>
									<p className="text-sm text-gray-500">
										Deployed KiteTail (249df660 in master)
										to staging
									</p>
								</div>
							</div>
						</li>
						<li className="py-4">
							<div className="flex space-x-3">
								<img
									className="h-6 w-6 rounded-full"
									src="https://images.unsplash.com/photo-1517365830460-955ce3ccd263?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=256&h=256&q=80"
									alt="a"
								/>
								<div className="flex-1 space-y-1">
									<div className="flex items-center justify-between">
										<h3 className="text-sm font-medium">
											You
										</h3>
										<p className="text-sm text-gray-500">
											12h
										</p>
									</div>
									<p className="text-sm text-gray-500">
										Deployed Workcation (11464223 in master)
										to staging
									</p>
								</div>
							</div>
						</li>
						<li className="py-4">
							<div className="flex space-x-3">
								<img
									className="h-6 w-6 rounded-full"
									src="https://images.unsplash.com/photo-1517365830460-955ce3ccd263?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=256&h=256&q=80"
									alt="a"
								/>
								<div className="flex-1 space-y-1">
									<div className="flex items-center justify-between">
										<h3 className="text-sm font-medium">
											You
										</h3>
										<p className="text-sm text-gray-500">
											2d
										</p>
									</div>
									<p className="text-sm text-gray-500">
										Deployed Easywire (dad28e95 in master)
										to production
									</p>
								</div>
							</div>
						</li>
						<li className="py-4">
							<div className="flex space-x-3">
								<img
									className="h-6 w-6 rounded-full"
									src="https://images.unsplash.com/photo-1517365830460-955ce3ccd263?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=256&h=256&q=80"
									alt="a"
								/>
								<div className="flex-1 space-y-1">
									<div className="flex items-center justify-between">
										<h3 className="text-sm font-medium">
											You
										</h3>
										<p className="text-sm text-gray-500">
											5d
										</p>
									</div>
									<p className="text-sm text-gray-500">
										Deployed Easywire (624bc94c in master)
										to production
									</p>
								</div>
							</div>
						</li>
						<li className="py-4">
							<div className="flex space-x-3">
								<img
									className="h-6 w-6 rounded-full"
									src="https://images.unsplash.com/photo-1517365830460-955ce3ccd263?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=256&h=256&q=80"
									alt="a"
								/>
								<div className="flex-1 space-y-1">
									<div className="flex items-center justify-between">
										<h3 className="text-sm font-medium">
											You
										</h3>
										<p className="text-sm text-gray-500">
											1w
										</p>
									</div>
									<p className="text-sm text-gray-500">
										Deployed Workcation (e111f80e in master)
										to production
									</p>
								</div>
							</div>
						</li>
						<li className="py-4">
							<div className="flex space-x-3">
								<img
									className="h-6 w-6 rounded-full"
									src="https://images.unsplash.com/photo-1517365830460-955ce3ccd263?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=256&h=256&q=80"
									alt="a"
								/>
								<div className="flex-1 space-y-1">
									<div className="flex items-center justify-between">
										<h3 className="text-sm font-medium">
											You
										</h3>
										<p className="text-sm text-gray-500">
											1w
										</p>
									</div>
									<p className="text-sm text-gray-500">
										Deployed Resumaid (5e136005 in master)
										to staging
									</p>
								</div>
							</div>
						</li>
						<li className="py-4">
							<div className="flex space-x-3">
								<img
									className="h-6 w-6 rounded-full"
									src="https://images.unsplash.com/photo-1517365830460-955ce3ccd263?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=256&h=256&q=80"
									alt="a"
								/>
								<div className="flex-1 space-y-1">
									<div className="flex items-center justify-between">
										<h3 className="text-sm font-medium">
											You
										</h3>
										<p className="text-sm text-gray-500">
											2w
										</p>
									</div>
									<p className="text-sm text-gray-500">
										Deployed KiteTail (5c1fd07f in master)
										to production
									</p>
								</div>
							</div>
						</li>
					</ul>
					<div className="py-4 text-sm border-t border-gray-200">
						<a
							href="#!"
							className="text-indigo-600 font-semibold hover:text-indigo-900"
						>
							View all activity <span aria-hidden="true">→</span>
						</a>
					</div>
				</div>
			</div>
		</div>
	);
};
