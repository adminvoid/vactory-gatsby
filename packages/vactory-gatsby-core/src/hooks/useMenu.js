import {default as menusData} from '../../.tmp/menus'
import {useTranslation} from "react-i18next";

const flatMenu = (array) => {
    var result = [];
    array.forEach(function (a) {
        result.push(a);
        if (Array.isArray(a.below)) {
            result = result.concat(flatMenu(a.below));
        }
    });
    return result;
}

const mergeI18nMenu = (array = [], list = []) => {
    var result = [];

    array.forEach(function (a) {
        a.url = list.find(fitem => fitem.id === a.id)?.url || ''
        if (Array.isArray(a.below)) {
            a.below = [].concat(mergeI18nMenu(a.below, list));
        }
        result.push(a);
    });
    return result;
}



export const useMenu = (menuID) => {
    const {i18n} = useTranslation();
    let menu = menusData.menus.find(item => item.menu_name === menuID && item.locale === i18n.language);
    if (i18n.language === 'Tfng' && menu) {
        const menu_ar = menusData.menus.find(item => item.menu_name === menuID && item.locale === 'ar');
        const flatten_menu = flatMenu(JSON.parse(menu_ar.items));
        const original_menu = JSON.parse(menu.items) || [];
        let tfng_menu = mergeI18nMenu(original_menu, flatten_menu);
        return tfng_menu || [];
    }
    return menu ? JSON.parse(menu.items) : [];
};
